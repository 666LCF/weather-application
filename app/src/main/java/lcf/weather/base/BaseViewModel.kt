package lcf.weather.base

import androidx.lifecycle.ViewModel

abstract class BaseViewModel : ViewModel()