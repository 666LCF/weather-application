package lcf.weather.weather_cities

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import lcf.weather.R
import lcf.weather.base.BaseFragment
import lcf.weather.databinding.FragmentWeatherCitiesBinding
import lcf.weather.domain.model.CityEntity
import lcf.weather.domain.model.Output

class WeatherCitiesFragment: BaseFragment() {

    private val citiesViewModel: WeatherCitiesViewModel by viewModels()
    private var binding: FragmentWeatherCitiesBinding? = null
    private lateinit var citiesAdapter: WeatherCitiesAdapter

    private var citiesList: MutableList<CityEntity> = arrayListOf()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentWeatherCitiesBinding.inflate(inflater, container, false).let {
        binding = it
        with(it) {
            headerTitle = "Weather App"
            root
        }
    }

    override fun subscribeUi() {
        citiesList.add(CityEntity("Almaty", 43.238949, 76.889709, arrayListOf()))
        citiesList.add(CityEntity("Astana", 51.169392, 71.449074, arrayListOf()))
        citiesList.add(CityEntity("Shymkent", 42.340782, 69.596329, arrayListOf()))

        binding?.let {
            citiesAdapter = WeatherCitiesAdapter(arrayListOf(), onCityClick)
            it.rvCities.adapter = citiesAdapter
            it.swipeRefresh.setOnRefreshListener {
                citiesViewModel.getTemperatures(citiesList)
            }

            citiesViewModel.getTemperatures(citiesList)

            citiesViewModel.citiesList.observe(viewLifecycleOwner) { result ->
                if (result != null) {
                    binding?.swipeRefresh?.isRefreshing = false
                    citiesAdapter.update(result)
                }
            }
        }
    }

    private val onCityClick: (city: CityEntity, view: View) -> Unit =
        { city, view ->
//            val extras = FragmentNavigatorExtras(
//                view to city
//            )
//            findNavController().navigate(
//                R.id.stars_to_details,
//                StarDetailsFragment.Args(star).toBundle(),
//                null,
//                extras
//            )
        }

}