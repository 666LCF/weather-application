package lcf.weather.weather_cities

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import lcf.weather.base.BaseViewModel
import lcf.weather.domain.model.*
import lcf.weather.domain.usecase.TemperaturesUseCase
import javax.inject.Inject

@HiltViewModel
class WeatherCitiesViewModel @Inject constructor(
    private val temperaturesUseCase: TemperaturesUseCase
): BaseViewModel() {
    private val _citiesList = MutableLiveData<List<CityEntity>>()
    val citiesList: LiveData<List<CityEntity>> = _citiesList

//    init {
//        getTemperatures()
//    }

    fun getTemperatures(citiesList: MutableList<CityEntity>) {
        val updatedCityList = citiesList
        citiesList.forEachIndexed { index, cityEntity ->
            viewModelScope.launch {
                temperaturesUseCase.execute(cityEntity.lat, cityEntity.lng).collect {output ->
                    val listIntervals : ArrayList<IntervalEntity> = arrayListOf()

                    output?.data?.timelines?.get(0).let {
                        if (!it?.intervals.isNullOrEmpty()) {
                            it?.intervals?.forEachIndexed { index, intervalEntity ->
                                listIntervals.add(intervalEntity)
                                Log.e("BLABLABLA", intervalEntity.values.getTemperatureString())
                            }
                        }
                    }
                    updatedCityList[index].intervals.clear()
                    updatedCityList[index].intervals.addAll(listIntervals)
                    _citiesList.value = updatedCityList
                }
            }
        }
    }
}