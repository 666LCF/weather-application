package lcf.weather.weather_cities

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import lcf.weather.databinding.ItemCityBinding
import lcf.weather.domain.model.CityEntity

class WeatherCitiesAdapter(
    private val list: ArrayList<CityEntity>,
    private val onWeatherCityClick: (details: CityEntity, view: View) -> Unit
) : RecyclerView.Adapter<WeatherCitiesAdapter.WeatherCityHolder>() {

    inner class WeatherCityHolder(private val binding: ItemCityBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: CityEntity) {
            binding.item = item
            binding.position = adapterPosition
            binding.root.setOnClickListener {
                onWeatherCityClick.invoke(item, binding.root)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherCityHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemCityBinding.inflate(inflater, parent, false)
        return WeatherCityHolder(binding)
    }

    override fun onBindViewHolder(holder: WeatherCityHolder, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount() = list.size

    fun update(newList: List<CityEntity>) {
        list.clear()
        list.addAll(newList)
        notifyItemRangeChanged(0, list.size)
    }
}