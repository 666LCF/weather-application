package lcf.weather.domain.model

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.parcelize.Parcelize

@Keep
@Parcelize
data class TemperatureEntity(
    val temperature: Double
) : Parcelable {
    fun getTemperatureString() : String {
      return temperature.toString()
    }
}