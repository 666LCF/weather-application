package lcf.weather.domain.model

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.parcelize.Parcelize

@Keep
@Parcelize
data class IntervalEntity(
    val startTime: String,
    val values: TemperatureEntity
): Parcelable