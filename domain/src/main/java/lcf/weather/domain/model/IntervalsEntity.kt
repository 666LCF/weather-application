package lcf.weather.domain.model

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.parcelize.Parcelize

@Keep
@Parcelize
data class IntervalsEntity(
    val timestep: String,
    val endTime: String,
    val startTime: String,
    val intervals: List<IntervalEntity>
) : Parcelable