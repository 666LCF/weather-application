package lcf.weather.domain.model

class CityEntity(
    var nameCity: String,
    var lat: Double,
    var lng: Double,
    var intervals: ArrayList<IntervalEntity> = arrayListOf()
) {
    fun getTemperature() : String {
        return if (intervals.isEmpty())
            "0.0 C"
        else intervals[0].values.getTemperatureString()
    }
}