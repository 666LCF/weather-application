package lcf.weather.domain.model

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.parcelize.Parcelize
import org.w3c.dom.Entity
import java.sql.Time

@Keep
@Parcelize
data class Output(
    val status: Status,
    val data: TimelineEntity?
) : Parcelable {

    enum class Status {
        SUCCESS,
        ERROR,
        LOADING
    }

    companion object {
        fun success(data: TimelineEntity): Output {
            return Output(Status.SUCCESS, data)
        }

        fun error(message: String, error: ApiError?): Output {
            return Output(Status.ERROR, null)
        }

        fun loading(data: TimelineEntity? = null): Output {
            return Output(Status.LOADING, data)
        }
    }

    override fun toString(): String {
        return "Result(status=$status, data=$data)"
    }
}