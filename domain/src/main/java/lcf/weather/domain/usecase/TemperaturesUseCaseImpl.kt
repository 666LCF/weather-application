package lcf.weather.domain.usecase

import kotlinx.coroutines.flow.Flow
import lcf.weather.domain.model.Output
import lcf.weather.domain.model.TimelineEntity
import lcf.weather.domain.repository.TemperaturesRepository
import javax.inject.Inject

internal class TemperaturesUseCaseImpl @Inject constructor(private val temperaturesRepository: TemperaturesRepository) : TemperaturesUseCase {
    override suspend fun execute(lat: Double, lng: Double): Flow<Output> {
        return temperaturesRepository.getTemperatures(lat, lng)
    }
}