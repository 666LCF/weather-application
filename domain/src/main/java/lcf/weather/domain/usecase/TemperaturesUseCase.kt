package lcf.weather.domain.usecase

import kotlinx.coroutines.flow.Flow
import lcf.weather.domain.model.IntervalEntity
import lcf.weather.domain.model.Output
import lcf.weather.domain.model.TimelineEntity

interface TemperaturesUseCase {
//    suspend fun execute(): Flow<Output?>
    suspend fun execute(lat: Double,lng: Double): Flow<Output>
}