package lcf.weather.domain.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import lcf.weather.domain.usecase.TemperaturesUseCase
import lcf.weather.domain.usecase.TemperaturesUseCaseImpl
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class UseCaseModule {
    @Binds
    @Singleton
    internal abstract fun bindTemperatureUseCase(useCaseImpl: TemperaturesUseCaseImpl): TemperaturesUseCase
}