package lcf.weather.domain.repository

import kotlinx.coroutines.flow.Flow
import lcf.weather.domain.model.IntervalEntity
import lcf.weather.domain.model.Output
import lcf.weather.domain.model.TimelineEntity

interface TemperaturesRepository {
    suspend fun getTemperatures(lat: Double,lng: Double) : Flow<Output>
}