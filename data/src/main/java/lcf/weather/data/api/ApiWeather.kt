package lcf.weather.data.api

import lcf.weather.domain.model.Output
import lcf.weather.domain.model.TimelineEntity
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiWeather {

    @GET("v4/timelines?fields=temperature&timesteps=1d&units=metric&apikey=5WMmLlZX7p92q5BJRMPuoqtIMWwEwFuR&startTime=2022-12-02T12:00:00Z&endTime=2022-12-15T06:00:00Z")
    suspend fun getTemperatures(
        @Query("location") location: String
    ): Response<Output>
}