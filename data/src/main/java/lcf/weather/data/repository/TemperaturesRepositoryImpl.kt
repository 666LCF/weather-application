package lcf.weather.data.repository

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import lcf.weather.data.remote.TemperaturesRemoteDataSource
import lcf.weather.domain.model.Output
import lcf.weather.domain.model.TimelineEntity
import lcf.weather.domain.repository.TemperaturesRepository
import javax.inject.Inject

internal class TemperaturesRepositoryImpl @Inject constructor(
    private val temperaturesRemoteDataSource: TemperaturesRemoteDataSource
) : TemperaturesRepository {
    override suspend fun getTemperatures(lat: Double, lng: Double): Flow<Output> {
        return flow{
            emit(Output.loading())
            val result = temperaturesRemoteDataSource.getTemperatures(listOf(lat, lng).joinToString())
            emit(result)
        }.flowOn(Dispatchers.IO)
    }
}