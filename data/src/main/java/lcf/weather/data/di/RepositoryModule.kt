package lcf.weather.data.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import lcf.weather.data.repository.TemperaturesRepositoryImpl
import lcf.weather.domain.repository.TemperaturesRepository
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {
    @Binds
    @Singleton
    internal abstract fun bindTemperaturesRepository(repositoryImpl: TemperaturesRepositoryImpl) : TemperaturesRepository
}