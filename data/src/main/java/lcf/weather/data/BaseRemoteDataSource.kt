package lcf.weather.data

import lcf.weather.domain.model.ApiError
import lcf.weather.domain.model.Output
import retrofit2.Response
import retrofit2.Retrofit
import java.io.IOException

abstract class BaseRemoteDataSource constructor(
    private val retrofit: Retrofit
) {

    suspend fun getResponse(
        request: suspend () -> Response<Output>,
        defaultErrorMessage: String
    ): Output {
        return try {
            println("I'm working in thread ${Thread.currentThread().name}")
            val result = request.invoke()
            if (result.isSuccessful) {
                return Output.success(result.body()?.data!!)
            } else {
                val errorResponse = parseError(result)
                Output.error(errorResponse?.statusMessage ?: defaultErrorMessage, errorResponse)
            }
        } catch (e: Throwable) {
            Output.error("Unknown Error", null)
        }
    }

    private fun parseError(response: Response<*>): ApiError? {
        val converter =
            retrofit.responseBodyConverter<ApiError>(ApiError::class.java, arrayOfNulls(0))
        return try {
            response.errorBody()?.let {
                converter.convert(it)
            }
        } catch (e: IOException) {
            ApiError()
        }
    }
}