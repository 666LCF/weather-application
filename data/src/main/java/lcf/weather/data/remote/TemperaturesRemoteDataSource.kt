package lcf.weather.data.remote

import lcf.weather.data.BaseRemoteDataSource
import lcf.weather.data.api.ApiWeather
import lcf.weather.domain.model.Output
import lcf.weather.domain.model.TimelineEntity
import retrofit2.Retrofit
import javax.inject.Inject

class TemperaturesRemoteDataSource @Inject constructor(
    private val apiWeather: ApiWeather, retrofit: Retrofit
) : BaseRemoteDataSource(retrofit) {

    suspend fun getTemperatures(location: String): Output {
        return getResponse(
            request = {apiWeather.getTemperatures(location)},
            defaultErrorMessage = "Error getting Temperatures"
        )
    }
}